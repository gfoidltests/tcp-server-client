﻿#define BIG_ENDIAN_     // C# Server by default uses only little endian

#include <iostream>
#include "serialization.h"

using std::cout;
using std::endl;

unsigned char* serialize(unsigned char* buffer, const int value)
{
#ifdef BIG_ENDIAN_
    cout << "Big Endian" << endl;

    // big endian -> is not necessary, data can be read with BigEndian but maybe this is automatically changed by socket?!
    buffer[0] = value >> 24;
    buffer[1] = value >> 16;
    buffer[2] = value >> 8;
    buffer[3] = value;
#else
    cout << "Little Endian" << endl;

    buffer[0] = value;
    buffer[1] = value >> 8;
    buffer[2] = value >> 16;
    buffer[3] = value >> 24;
#endif
    return buffer + 4;
}
//-----------------------------------------------------------------------------
unsigned char* serialize(unsigned char* buffer, const arguments& args)
{
    buffer = serialize(buffer, args.a);
    buffer = serialize(buffer, args.b);

    return buffer;
}
//-----------------------------------------------------------------------------
unsigned char* deSerialize(const unsigned char* buffer, int& value)
{
#ifdef BIG_ENDIAN_
    value = (buffer[0] << 24 | buffer[1] << 16 | buffer[2] << 8 | buffer[3]);
#else
    value = (buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24);
#endif

    return (unsigned char*)buffer + 4;
}
//-----------------------------------------------------------------------------
unsigned char* deSerialize(const unsigned char* buffer, arguments& args)
{
    buffer = deSerialize(buffer, args.a);
    buffer = deSerialize(buffer, args.b);

    return (unsigned char*)buffer;
}