﻿#define SERIALIZE           // casting the "object" direct to char* -> transport as is, i.e. little endian on intel machines
//#define SIMPLE

#include <iostream>
#include <string>
#include "TcpClient.h"
#include "types.h"
#include "serialization.h"

using namespace std;

//-----------------------------------------------------------------------------
bool IsBigEndian();
int Run(const string& hostname, const USHORT port);
void PrintServerReply(const string& reply);
//-----------------------------------------------------------------------------
int main(int argc, char** argv)
{
    cout << (IsBigEndian() ? "big" : "little") << " endian" << endl << endl;
    
    string hostname   = "localhost";
    const USHORT port = 8080;

    if (argc > 1)
        hostname = argv[1];

    cout << "Waiting for server to run. Hit <RETURN> to continue." << endl;
    cin.get();

    return Run(hostname, port);
}
//-----------------------------------------------------------------------------
bool IsBigEndian()
{
    union
    {
        unsigned int i;
        char c[4];
    } end = { 0x01020304 };

    return end.c[0] == 1;
}
//-----------------------------------------------------------------------------
int Run(const string& hostname, const USHORT port)
{
    IPC::TcpClient tcpClient;
    string response;

    if (!tcpClient.connect(hostname, 8080, true))
        return 1;

    if (!tcpClient.send("Hello")) return 1;
    response = tcpClient.receive();
    PrintServerReply(response);

    cout << endl << "hit <RETURN> for next step" << endl;
    cin.get();

    if (!tcpClient.send("foo")) return 1;
    response = tcpClient.receive();
    PrintServerReply(response);

    //if (!tcpClient.send("LongText")) return 1;
    //response = tcpClient.receive(true);
    //PrintServerReply(response);

    if (!tcpClient.send("Add")) return 1;
    response = tcpClient.receive();
    PrintServerReply(response);
#ifndef SIMPLE
    arguments args = { 0xf000, 0x0001 };
#else
    arguments args = { 1, 2 };
#endif
    unsigned char buffer[32];
#ifndef SERIALIZE
    if (!tcpClient.send((char*)&args, sizeof(arguments))) return 1;
#else
    unsigned char* ptr;
    ptr = serialize(buffer, args);
    if (!tcpClient.send((char*)buffer, ptr - buffer)) return 1;
#endif
    if (!tcpClient.receive((char*)buffer, 4)) return 1;

    int sum = *(int*)buffer;
    PrintServerReply(to_string(sum));

    if (!tcpClient.send("Swap")) return 1;
    response = tcpClient.receive();
    PrintServerReply(response);
#ifndef SERIALIZE
    if (!tcpClient.send((char*)&args, sizeof(arguments), true)) return 1;
#else
    ptr = serialize(buffer, args);
    if (!tcpClient.send((char*)buffer, ptr - buffer)) return 1;
#endif
    if (!tcpClient.receive((char*)buffer, 8, true)) return 1;
#ifndef SERIALIZE
    args = *(arguments*)buffer;
#else
    ptr = deSerialize(buffer, args);
#endif
    PrintServerReply("a = " + to_string(args.a) + ", b = " + to_string(args.b));

    tcpClient.disconnect();

    return 0;
}
//-----------------------------------------------------------------------------
void PrintServerReply(const string& reply)
{
    cout << "Received from server> " << reply << endl;
}