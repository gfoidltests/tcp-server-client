﻿#pragma once

#include "types.h"

unsigned char* serialize(unsigned char* buffer, const int value);
unsigned char* serialize(unsigned char* buffer, const arguments& args);
unsigned char* deSerialize(unsigned const char* buffer, int& value);
unsigned char* deSerialize(unsigned const char* buffer, arguments& args);