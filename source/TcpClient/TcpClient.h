﻿#ifndef TcpClient_h
#define TcpClient_h

#include <string>

#ifdef WIN32
    #include <WinSock2.h>
    #pragma comment(lib, "ws2_32.lib")      // Winsock library
#else
    #include <arpa/inet.h>

    typedef unsigned short USHORT;
#endif

using namespace std;

namespace IPC
{
    class TcpClient
    {
    private:
        USHORT _port;
        string _address;
#ifdef WIN32
        WSADATA _wsa;
        SOCKET _socket;
#else
        int _socket;
#endif
        struct sockaddr_in _server;

    public:
        TcpClient();
        ~TcpClient();
        bool connect(const string&, const USHORT, const bool printAddress = false);
        void disconnect();
        bool send(const char* buffer, const unsigned int bufLen, const bool shutdownSocketAfterSend = false);
        bool send(const string& data, const bool shutdownSocketAfterSend = false);
        bool receive(char* buffer, const unsigned int expected, const bool shutdownSocketAfterReceive = false);
        string receive(const bool shutdownSocketAfterReceive = false, const unsigned int = 512, const string& terminator = "#END#");
    };
}

#endif