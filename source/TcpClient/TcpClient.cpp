﻿#include <iostream>
#include <stdio.h>
#include <string.h>
#include "TcpClient.h"

#ifndef WIN32
    #define INVALID_SOCKET  -1
    #define SOCKET_ERROR    -1
    #define SD_RECEIVE      SHUT_RD
    #define SD_SEND         SHUT_WR
    #define SD_BOTH         SHUT_RDWR

    #include <sys/socket.h>
    #include <netdb.h>
    #include <cstdlib>
#endif

namespace IPC
{
    bool getAddressFromHostName(const string& hostname, in_addr& addr, const bool printResult = false);
    bool hasEnding(const string& fullString, const string& ending);
    bool isTerminated(string& fullString, const string& ending, bool cleanTerminator = true);
    //-------------------------------------------------------------------------
    TcpClient::TcpClient()
    {
        _socket  = -1;
        _port    = 0;
        _address = "";
    }
    //-------------------------------------------------------------------------
    TcpClient::~TcpClient()
    {
        //this->disconnect();
        disconnect();
    }
    //-------------------------------------------------------------------------
    bool TcpClient::connect(const string& address, const USHORT port, const bool printAddress)
    {
        if (_socket == -1)
        {
#ifdef WIN32
            if (WSAStartup(MAKEWORD(2, 2), &_wsa) != 0)
            {
                perror("Could not init winsock2");
                WSACleanup();
                return false;
            }
#endif
            _socket = socket(AF_INET, SOCK_STREAM, 0);
            if (_socket == INVALID_SOCKET)
            {
                perror("Could not create socket");
                return false;
            }
        }

        in_addr addr;
        if (!getAddressFromHostName(address, addr, printAddress))
        {
            cerr << "Error in resolving address" << endl;
            return false;
        }
        _server.sin_addr   = addr;
        _server.sin_family = AF_INET;
        _server.sin_port   = htons(port);           // for byte ordering (Little Endian vs. Big Endian)

        // ::connect because this method has the same name, and the compiler doesn't recognize this
        if (::connect(_socket, (struct sockaddr*)&_server, sizeof(_server)) < 0)
        {
            cerr << "connect failed" << endl;
            return false;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    void TcpClient::disconnect()
    {
        if (_socket == -1) return;

#ifdef WIN32
        closesocket(_socket);
        WSACleanup();
#else
        //close(_socket);
#endif
        _socket = -1;
    }
    //-------------------------------------------------------------------------
    bool TcpClient::send(const char* buffer, const unsigned int bufLen, const bool shutdownSocketAfterSend)
    {
        if (::send(_socket, buffer, bufLen, 0) < 0)
        {
#ifdef WIN32
            cerr << "Send failed: " << WSAGetLastError() << endl;
#else
            cerr << "Send failed" << endl;
#endif
            return false;
        }

        if (shutdownSocketAfterSend)
            shutdown(_socket, SD_SEND);

        return true;
    }
    //-------------------------------------------------------------------------
    bool TcpClient::send(const string& data, const bool shutdownSocketAfterSend)
    {
        string tmp = data;

        if (tmp.back() != '\n')
            tmp.append("\n");

        return send(tmp.c_str(), strlen(tmp.c_str()), shutdownSocketAfterSend);
    }
    //-------------------------------------------------------------------------
    bool TcpClient::receive(char* buffer, const unsigned int expected, const bool shutdownSocketAfterReceive)
    {
        int read(0);

        while (true)
        {
            read += recv(_socket, buffer, expected, 0);

            if (read == expected) break;

            if (read < 0)
            {
#if WIN32
                cerr << "recv failed: " << WSAGetLastError() << endl;
#else
                cerr << "recv failed." << endl;
#endif
                break;
            }
        }

        if (shutdownSocketAfterReceive)
            shutdown(_socket, SD_RECEIVE);

        return read == expected;
    }
    //-------------------------------------------------------------------------
    string TcpClient::receive(const bool shutdownSocketAfterReceive, const unsigned int bufferSize, const string& terminator)
    {
#ifdef WIN32
        char buffer[512];
#else   // g++ checks it, VC++ not :-(
        char buffer[bufferSize];
#endif
        bool isEnd(false);
        int totalSize(0);       // (0) is direct initialization. = 0 is copy assignment
        string reply;

        do
        {
            int read = recv(_socket, buffer, bufferSize, 0);

            if (read > 0)
            {
                string tmp(buffer, read);

                isEnd = isTerminated(tmp, terminator);

                totalSize += read;
                reply.append(tmp);
            }
            else if (read == 0)
            {
                cout << "Connection closed" << endl;
                isEnd = true;
            }
            else
#if WIN32
                cerr << "recv failed: " << WSAGetLastError() << endl;
#else
                cerr << "recv failed." << endl;
#endif
        } while (!isEnd);

        if (shutdownSocketAfterReceive)
            shutdown(_socket, SD_RECEIVE);

        return reply;
    }
    //-------------------------------------------------------------------------
    bool getAddressFromHostName(const string& hostname, in_addr& addr, const bool printResult)
    {
        struct hostent* he;
        struct in_addr** addr_list;

        if ((he = gethostbyname(hostname.c_str())) == NULL)
        {
#ifdef WIN32
            cerr << "gethostbyname failed for " << hostname << ": " << WSAGetLastError() << endl;
#else
            cerr << "gethostbyname failed for " << hostname << endl;
#endif
            return false;
        }

        addr_list = (struct in_addr**)he->h_addr_list;

        addr = *addr_list[0];

        if (printResult)
        {
            char ip[100];

            for (int i = 0; addr_list[i] != NULL; ++i)
#ifdef WIN32
                strcpy_s(ip, inet_ntoa(*addr_list[i]));
#else
                strcpy(ip, inet_ntoa(*addr_list[i]));
#endif

            cout << hostname << " resolved to " << ip << endl;
        }

        return true;
    }
    //-------------------------------------------------------------------------
    bool hasEnding(const string& fullString, const string& ending)
    {
        if (fullString.length() < ending.length()) return false;

        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }
    //-------------------------------------------------------------------------
    bool isTerminated(string& fullString, const string& ending, bool cleanTerminator)
    {
        if (!cleanTerminator) return hasEnding(fullString, ending);

        string::size_type i = fullString.find(ending);

        if (i != string::npos)
        {
            fullString.erase(i, ending.length());
            return true;
        }

        return false;
    }
}