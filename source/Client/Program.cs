﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Server;

namespace Client
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting for server. Any key to continue");
            Console.ReadKey();

            string serverAddress = "localhost";

            if (args.Length > 0) serverAddress = args[0];

            using (TcpClient client = new TcpClient(serverAddress, 8080))
            {
                var stream = client.GetStream();

                StreamWriter sw = new StreamWriter(stream);
                StreamReader sr = new StreamReader(stream);
                string response = null;

                sw.WriteLine("HELLO");
                sw.Flush();
                response = ReadFromServer(stream);
                PrintServerReply(response);

                Console.WriteLine("hit any key for next step");
                Console.ReadKey();

                sw.WriteLine("foo");
                sw.Flush();
                response = ReadFromServer(stream);
                PrintServerReply(response);

                //sw.WriteLine("LongText");
                //sw.Flush();
                //response = ReadFromServer(stream);
                //PrintServerReply(response);

                sw.WriteLine("Add");
                sw.Flush();
                response = ReadFromServer(stream);
                PrintServerReply(response);
                BinaryWriter bw = new BinaryWriter(stream);
                BinaryReader br = new BinaryReader(stream);
                Arguments arguments = new Arguments { A = 0xf000, B = 0x0001 };
                Serializer ser = new Serializer(true);
                ser.Serialize(stream, arguments);
                stream.Flush();
                while (!stream.DataAvailable)
                    Thread.Sleep(50);
                int res = br.ReadInt32();
                PrintServerReply(res.ToString());

                sw.WriteLine("Swap");
                sw.Flush();
                response = ReadFromServer(stream);
                PrintServerReply(response);
                ser.Serialize(stream, arguments);
                stream.Flush();
                client.Client.Shutdown(SocketShutdown.Send);
                while (!stream.DataAvailable)
                    Thread.Sleep(50);
                arguments = ser.DeSerialize(stream);
                client.Client.Shutdown(SocketShutdown.Receive);
                PrintServerReply($"a = {arguments.A}, b = {arguments.B}");

                client.Close();
            }

            Console.WriteLine("\nEnd.");
            Console.ReadKey();
        }
        //---------------------------------------------------------------------
        private static string ReadFromServer(NetworkStream stream, string terminator = "#END#", int bufferSize = 1024)
        {
            bool isEnd       = false;
            byte[] buffer    = new byte[bufferSize];
            StringBuilder sb = new StringBuilder();

            do
            {
                int read   = stream.Read(buffer, 0, buffer.Length);
                string tmp = Encoding.UTF8.GetString(buffer, 0, read);

                isEnd = tmp.EndsWith(terminator);

                if (isEnd) tmp = tmp.Replace(terminator, string.Empty);

                sb.Append(tmp);
            } while (!isEnd);

            return sb.ToString();
        }
        //---------------------------------------------------------------------
        private static void PrintServerReply(string temp)
        {
            Console.Write("Received from server> ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(temp);
            Console.ResetColor();
        }
    }
}