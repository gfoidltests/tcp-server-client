﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server
{
    public class TcpServer : IDisposable
    {
        private const int Port = 8080;
        private readonly TcpListener _listener;
        private readonly Dispatcher  _dispatcher;
        private volatile bool        _isRunning = false;
        private int                  _clientID = -1;
        //---------------------------------------------------------------------
        public TcpServer()
        {
            _dispatcher = new Dispatcher();
            //_listener = new TcpListener(IPAddress.Any, Port);
            _listener   = TcpListener.Create(Port);
            _listener.Start();

            Console.WriteLine($"Server started on port {Port}");
        }
        //---------------------------------------------------------------------
        public void Run()
        {
            this.ThrowIfDisposed();

            _isRunning = true;

            Task.Factory.StartNew(async () =>
            {
                try
                {
                    while (_isRunning)
                    {
                        TcpClient client = await _listener.AcceptTcpClientAsync().ConfigureAwait(false);

                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"Client {++_clientID} connected");
                        Console.ResetColor();

                        this.HandelClientAsync(client, _clientID);
                    }
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Error.WriteLine(ex.Message);
                    Console.ResetColor();
                }
            }, TaskCreationOptions.LongRunning);
        }
        //---------------------------------------------------------------------
        private async void HandelClientAsync(TcpClient tcpClient, int clientID)
        {
            var stream = tcpClient.GetStream();

            while (_isRunning && tcpClient.Connected)
            {
                bool state = await _dispatcher.DispatchAsync(stream).ConfigureAwait(false);

                if (!state) break;
            }

            // Server shall not close connection, this is up to the client,
            // so it is not just Request / Response, but multiple Req/Res.
            //if (tcpClient.Connected)
            //  tcpClient.Close();

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine($"Client {clientID} disconnected");
            Console.ResetColor();
        }
        //---------------------------------------------------------------------
        public void Stop()
        {
            this.ThrowIfDisposed();

            _isRunning = false;

            // Close pending clients
            while (_listener.Pending())
            {
                var client = _listener.AcceptTcpClient();
                client.Close();
            }

            _listener.Stop();
        }
        //---------------------------------------------------------------------
        #region IDisposable Members
        private bool _isDisposed = false;
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        protected void ThrowIfDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException(this.ToString());
        }
        //---------------------------------------------------------------------
        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing && _isRunning)
                    this.Stop();

                _isDisposed = true;
            }
        }
        //---------------------------------------------------------------------
        [System.Diagnostics.DebuggerStepThrough]
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}