﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server
{
    public class Dispatcher
    {
        public async Task<bool> DispatchAsync(NetworkStream stream)
        {
            var sr = new StreamReader(stream);
            var sw = new StreamWriter(stream);

            string command = await sr.ReadLineAsync().ConfigureAwait(false);

            if (command == null) return false;

            var handler = new Handler(sw);

            if (string.Equals(command, "Hello", StringComparison.OrdinalIgnoreCase))
                await handler.HelloAsync().ConfigureAwait(false);
            else if (string.Equals(command, "GetTime", StringComparison.OrdinalIgnoreCase))
                await handler.GetTimeAsync().ConfigureAwait(false);
            else if (string.Equals(command, "LongText", StringComparison.OrdinalIgnoreCase))
                await handler.LongTextAsync().ConfigureAwait(false);
            else if (string.Equals(command, "Add", StringComparison.OrdinalIgnoreCase))
            {
                sw.Write("OK");
                await Finish(sw).ConfigureAwait(false);

                while (!stream.DataAvailable)
                    await Task.Delay(50);

                handler.Add(stream);
                await stream.FlushAsync().ConfigureAwait(false);
                return true;
            }
            else if (string.Equals(command, "Swap", StringComparison.OrdinalIgnoreCase))
            {
                sw.Write("OK");
                await Finish(sw).ConfigureAwait(false);

                while (!stream.DataAvailable)
                    await Task.Delay(50);

                handler.Swap(stream);
                await stream.FlushAsync().ConfigureAwait(false);
                return true;
            }
            else
                await handler.HandleUnknownAsync().ConfigureAwait(false);

            await Finish(sw).ConfigureAwait(false);

            return true;
        }
        //---------------------------------------------------------------------
        private static async Task Finish(StreamWriter sw)
        {
            await sw.WriteAsync("#END#").ConfigureAwait(false);
            await sw.FlushAsync().ConfigureAwait(false);
        }
    }
}