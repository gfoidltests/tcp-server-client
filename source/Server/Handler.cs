﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server
{
    public class Handler
    {
        private readonly StreamWriter _sw;
        private readonly Serializer   _serializer = new Serializer(true);
        //---------------------------------------------------------------------
        public Handler(StreamWriter sw)
        {
            _sw = sw;
        }
        //---------------------------------------------------------------------
        public async Task HelloAsync()
        {
            await Task.Delay(250).ConfigureAwait(false);
            await _sw.WriteLineAsync($"Hello from Server {Environment.MachineName}").ConfigureAwait(false);
            await _sw.WriteLineAsync($"Time: {DateTime.Now}").ConfigureAwait(false);
        }
        //---------------------------------------------------------------------
        public async Task GetTimeAsync()
        {
            await _sw.WriteLineAsync($"Server-time: {DateTime.Now}").ConfigureAwait(false);
        }
        //---------------------------------------------------------------------
        public async Task LongTextAsync()
        {
            await _sw.WriteLineAsync(File.ReadAllText("longText.txt")).ConfigureAwait(false);
        }
        //---------------------------------------------------------------------
        public void Add(NetworkStream stream)
        {
            // http://stackoverflow.com/questions/1577161/passing-a-structure-through-sockets-in-c
            // http://stackoverflow.com/questions/6918545/how-to-read-byte-blocks-into-struct
            // http://www.codeproject.com/Articles/10750/Fast-Binary-File-Reading-with-C

            BinaryWriter bw = new BinaryWriter(stream);
            Arguments args  = _serializer.DeSerialize(stream);

            Console.WriteLine($"a = {args.A}, b = {args.B}");
            int sum = args.A + args.B;
            bw.Write(sum);
        }
        //---------------------------------------------------------------------
        public void Swap(NetworkStream stream)
        {
            Arguments args = _serializer.DeSerialize(stream);

            Console.WriteLine($"a = {args.A}, b = {args.B}");

            int tmp = args.B;
            args.B = args.A;
            args.A = tmp;

            Console.WriteLine($"a = {args.A}, b = {args.B}");

            _serializer.Serialize(stream, args);
        }
        //---------------------------------------------------------------------
        public async Task HandleUnknownAsync()
        {
            await _sw.WriteLineAsync("Unknown command").ConfigureAwait(false);
        }
    }
}