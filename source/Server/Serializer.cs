﻿//#define MARSHAL

using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Server
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Arguments
    {
        public int A;
        public int B;
    }
    //---------------------------------------------------------------------
    public class Serializer
    {
        private readonly bool _bigEndian;
        //---------------------------------------------------------------------
        public Serializer(bool bigEndian = true)
        {
            _bigEndian = bigEndian;
        }
        //---------------------------------------------------------------------
        public Arguments DeSerialize(Stream stream)
        {
            BinaryReader br = new BinaryReader(stream);
            Arguments args;
#if !MARSHAL
            if (_bigEndian)
            {
                byte[] b = br.ReadBytes(8);
                args.A = (b[0] << 24 | b[1] << 16 | b[2] << 8 | b[3]);
                args.B = (b[4] << 24 | b[5] << 16 | b[6] << 8 | b[7]);
            }
            else
            {
                args.A = br.ReadInt32();
                args.B = br.ReadInt32();
            }
#else
            byte[] buffer = br.ReadBytes(Marshal.SizeOf<Arguments>());

            if (_bigEndian) Array.Reverse(buffer);

            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            try
            {
                //args = (Arguments)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(Arguments));
                args = Marshal.PtrToStructure<Arguments>(handle.AddrOfPinnedObject());
            }
            finally
            {
                handle.Free();
            }
#endif
            return args;
        }
        //---------------------------------------------------------------------
        public void Serialize(Stream stream, Arguments args)
        {
            BinaryWriter bw = new BinaryWriter(stream);
#if !MARSHAL
            if (_bigEndian)
            {
                byte[] b =
                {
                    (byte)(args.A >> 24),
                    (byte)(args.A >> 16),
                    (byte)(args.A >> 8),
                    (byte)(args.A),

                    (byte)(args.B >> 24),
                    (byte)(args.B >> 16),
                    (byte)(args.B >> 8),
                    (byte)(args.B)
                };

                bw.Write(b);
            }
            else
            {
                bw.Write(args.A);
                bw.Write(args.B);
            }
#else
            byte[] buffer = new byte[Marshal.SizeOf<Arguments>()];
            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            try
            {
                Marshal.StructureToPtr(args, handle.AddrOfPinnedObject(), true);
            }
            finally
            {
                handle.Free();
            }

            if (_bigEndian) Array.Reverse(buffer);

            bw.Write(buffer);
#endif
        }
    }
}