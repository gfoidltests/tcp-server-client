﻿using System;

namespace Server
{
    static class Program
    {
        static void Main(string[] args)
        {
            using (TcpServer server = new TcpServer())
            {
                server.Run();

                Console.WriteLine("Server running. Press any key to quit.");
                Console.ReadKey();

                server.Stop();
            }
        }
    }
}